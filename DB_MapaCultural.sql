CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `area` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_cat` int,
  `asientos` varchar(50) NOT NULL,
  `pantalla` varchar(50) NULL,
  `amplitud` varchar(50) NULL,
  CONSTRAINT `FK_area_categoria` FOREIGN KEY (`id_cat`) REFERENCES `categoria`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;





 CREATE TABLE IF NOT EXISTS `localidad` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `cultura` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_cat` int ,
   CONSTRAINT `FK_espacioCultural_categoria` FOREIGN KEY (`id_cat`) REFERENCES `categoria`(`id`),
  `nombre` varchar(100) NOT NULL,
  `domicilio` varchar(60) NOT NULL,
  `piso` varchar(25) NULL,
  `año_inauguracion` year NOT NULL,
  `año_inicio` year NOT NULL,
  `observaciones` varchar(200) NULL,
  `informacion` varchar(200) NULL,
  `id_prov` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_provincia` FOREIGN KEY (`id_prov`) REFERENCES `provincia`(`id`),
  `id_loc` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_localidad` FOREIGN KEY (`id_loc`) REFERENCES `localidad`(`id`),
  `id_depart` int NOT NULL,
  CONSTRAINT `FK_espacioCultural_departamento` FOREIGN KEY (`id_depart`) REFERENCES `departamento`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cod_area` int(5) NULL,
  `telefono` int(5) NULL,
  `correo` varchar(200) NOT NULL,
  `web` varchar(200) NULL,
  `id_cul` INT,
  CONSTRAINT `FK_contacto_Cultural` FOREIGN KEY (`id_cul`) REFERENCES `cultura`(`id`),
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

